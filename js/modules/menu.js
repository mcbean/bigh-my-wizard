/**
 * Created by apple on 20/10/16.
 */

'use strict';

const {Menu, MenuItem} = require('electron');

const menu = new Menu()
menu.append(new MenuItem({
    label: 'MenuItem1',
    click() { console.log('item 1 clicked')}
}));
menu.append(new MenuItem({type: 'separator'}));
menu.append(new MenuItem({label: 'MenuItem2', type: 'checkbox', checked: true}));