/**
 * Created by apple on 18/10/16.
 */

'use strict';

const _REMOTE = require('electron').remote;
const _APP = _REMOTE.app;
const _SHELL = _REMOTE.shell;
const _OS = require('os');

/*
var usb = require('usb')
console.log(usb);
const tessel = require("tessel");
const sdcardlib = require("sdcard");
console.log(sdcardlib);
var sdcard = sdcardlib.use(tessel.port['A']);
*/

/*
console.log(_REMOTE);
console.log(_MENU);
var menu = new _MENU();
console.log(menu);
//_MENU.setApplicationMenu(_MENU.buildFromTemplate([  ]));

menu = _MENU.getApplicationMenu();
//menu.items[2].label = "Fika";

console.warn(menu.items);
console.warn(typeof menu);


console.error(_MENU.setApplicationMenu(menu));
*/

// Open links externally by default
$(document).on('click', 'a[href^="http"]', function(event) {
    event.preventDefault();
    _SHELL.openExternal(this.href);
});

// Add online/offline event
const {remote, ipcRenderer} = require('electron');
const curWin = remote.getCurrentWindow();

const updateOnlineStatus = () => {
    ipcRenderer.send('online-status-changed', navigator.onLine ? 'online' : 'offline')
}

window.addEventListener('online',  updateOnlineStatus)
window.addEventListener('offline',  updateOnlineStatus)

$(window).on("online", function(){
    $("#wizard-online-status").removeClass("alert-danger")
        .addClass("alert-success")
        .text("Il tuo computer è online");

    //console.log("online");
});

$(window).on("offline", function(){
    $("#wizard-online-status").removeClass("alert-success")
        .addClass("alert-danger")
        .text("Il tuo computer è offline");

    //console.log("offline");
});

$(document).ready(function(){

        $(".wizard-page").hide();
        $("#wizard-page0").show();
        $("#wizard-end").hide();
        $("#wizard-prev").addClass("disabled").attr("disabled",true).hide();

        // Check online status
        if(navigator.onLine){
            $(window).trigger("online");
        }
        else{
            $(window).trigger("offline");
        }

        //Get platform info
        $("#wizard-os-user").html(_OS.userInfo().username);
        $("#wizard-os-homedir").html(_OS.userInfo().homedir);

        $("#wizard-os-platform").html(_OS.release());
        $("#wizard-os-hostname").html(_OS.hostname());
        $("#wizard-os-cpus").html(_OS.cpus()[0].model);
        $("#wizard-os-type").html(_OS.type());
        $("#wizard-os-freemem").html(_OS.freemem());
        $("#wizard-os-endianness").html(_OS.endianness());
        $("#wizard-os-networkInterfaces").html(_OS.networkInterfaces());


        //console.log(_OS.userInfo());
        //console.log(_OS.networkInterfaces());
    }
);


$('#wizard-prev').on('click', function () {

    var $visiblePage = $(".wizard-page:visible");
    var strId = $visiblePage.attr("id");

    $("#wizard-end").hide();
    $("#wizard-next").show();
    $("#wizard-prev").removeClass("disabled").removeAttr("disabled").show();


    switch (strId){
        case "wizard-page0":
            break;
        case "wizard-page1":
            $(".wizard-page").hide();
            $("#wizard-page0").show();
            $("#wizard-prev").addClass("disabled").attr("disabled",true).hide();

            $("#wizard-next").removeClass("disabled").removeAttr("disabled");
            break;
        case "wizard-page2":
            $(".wizard-page").hide();
            $("#wizard-page1").show();

            // Progress bar complete
            $("#wizard-page1 .progress-bar").css('width', 100+'%').attr("aria-valuenow",100).removeClass("active");
            break;
        case "wizard-page3":
            $(".wizard-page").hide();
            $("#wizard-page2").show();
            break;
        case "wizard-page4":
            break;
    }
});

$('#wizard-next').on('click', function () {

    var $visiblePage = $(".wizard-page:visible");
    var strId = $visiblePage.attr("id");

    $("#wizard-end").hide();
    $("#wizard-next").show();
    $("#wizard-prev").removeClass("disabled").removeAttr("disabled").show();

    switch (strId){
        case "wizard-page0":
            $(".wizard-page").hide();
            $("#wizard-page1").show();
            $("#wizard-next").addClass("disabled").attr("disabled",true);

            // Progress bar start
            $("#wizard-page1 .progress-bar").css('width', 0+'%').attr("aria-valuenow",0).addClass("active");
            $(".wizard-installing-package").html("");

            var iValue = 0;
            curWin.setProgressBar(iValue);

            var oInterval = setInterval(function(){

                if(iValue >= 100){
                    clearInterval(oInterval);
                    $("#wizard-page1 .progress-bar").removeClass("active");
                    $("#wizard-next").removeClass("disabled").removeAttr("disabled");
                    $(".wizard-installing-package").html("Installation complete!");
                }
                else{
                    iValue += (Math.random()*20);
                    curWin.setProgressBar(iValue / 100);

                    //iValue = 100;
                    //console.log(iValue);

                    if(iValue>100){
                        iValue = 100;
                        curWin.setProgressBar(-1);
                    }

                    $("#wizard-page1 .progress-bar").css('width', iValue+'%').attr("aria-valuenow",iValue);

                    $(".wizard-installing-package").html("Installing PACKAGE_"+ parseInt((Math.random()*100),10) +"...");
                }

            }, 500);

            break;
        case "wizard-page1":
            $(".wizard-page").hide();
            $("#wizard-page2").show();
            break;
        case "wizard-page2":
            $(".wizard-page").hide();
            $("#wizard-page3").show();
            break;
        case "wizard-page3":
            $(".wizard-page").hide();
            $("#wizard-page4").show();
            $("#wizard-prev").hide();
            $("#wizard-next").hide();
            $("#wizard-end").show();
            break;
        case "wizard-page4":
            break;
    }
});

$('#wizard-exit, #wizard-end').on('click', function () {

    _APP.quit();

});

$('#wizard-notific').on('click', function () {

    let myNotification = new Notification('My (awesome) Wizard', {
        body: 'Questa è una notifica'
    });

    myNotification.onclick = function(){

        $("#wizard-notific-check").show();
        console.log('Notification clicked')
    };

});