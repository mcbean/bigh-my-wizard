'use strict';

const {app, BrowserWindow, Menu, MenuItem} = require('electron');

// Keep a global reference of the window object
let _WIN;

// Build window on app ready
app.on("ready", function(){
    var main = new BrowserWindow({
        //maximizable: false,
        //fullscreen: false,
        //maxWidth: 990,
        //maxHeight: 800,
        //show: false,
        //useContentSize: true,
        //transparent: true,
        //frame: false,
        //titleBarStyle: "hidden-inset",
        title: "My Wizard",
        width: 770,
        height: 600,
        minWidth: 770,
        minHeight: 600
    });


    main.loadURL("file://" + __dirname + "/index.html")
    main.webContents.openDevTools();

});